﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mx.Life.Storage
{
	public interface IStorage : IEnumerable<StorageKey>
	{
		bool this[StorageKey key]
		{
			get;
			set;
		}

		void Backup();

		bool Restore();
	}
}
