﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace Mx.Life.Storage
{
	internal sealed class MemoryStorage : IStorage
	{
		private readonly HashSet<StorageKey> _data = new HashSet<StorageKey>();

		public bool this[StorageKey key]
		{
			get
			{
				return _data.Contains(key);
			}
			set
			{
				if (value)
				{
					_data.Add(key);
				}
				else
				{
					_data.Remove(key);
				}
			}
		}

		public IEnumerator<StorageKey> GetEnumerator()
		{
			return _data.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		private string BackupFileName
		{
			get
			{
				var appFolder = AppDomain.CurrentDomain.BaseDirectory;
				var fileName = Path.Combine(appFolder, "game.mxlf");

				return fileName;
			}
		}

		public void Backup()
		{
			using (var fs = File.OpenWrite(BackupFileName))
			{
				var bw = new BinaryWriter(fs);
				bw.Write(_data.Count);

				foreach (var key in this)
				{
					bw.Write(key.Column);
					bw.Write(key.Row);
				}

				bw.Flush();
			}
		}

		public bool Restore()
		{
			this._data.Clear();

			if (!File.Exists(BackupFileName))
			{
				return false;
			}

			using (var fs = File.OpenRead(BackupFileName))
			{
				var br = new BinaryReader(fs);
				var count = br.ReadInt32();

				while (count > 0)
				{
					this._data.Add(new StorageKey(br.ReadUInt64(), br.ReadUInt64()));
					count--;
				}
			}

			return true;
		}
	}
}
