﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mx.Life.Storage
{
	public sealed class StorageKey : IEquatable<StorageKey>
	{
		public StorageKey(ulong column, ulong row)
		{
			Column = column;
			Row = row;
		}

		public ulong Column
		{
			get;
			private set;
		}

		public ulong Row
		{
			get;
			private set;
		}

		public bool Equals(StorageKey other)
		{
			if (other == null)
			{
				return false;
			}

			return other.Column == Column && other.Row == Row;
		}

		public override bool Equals(object obj)
		{
			return Equals(obj as StorageKey);
		}

		public override int GetHashCode()
		{
			return Column.GetHashCode() ^ Row.GetHashCode();
		}
	}
}
