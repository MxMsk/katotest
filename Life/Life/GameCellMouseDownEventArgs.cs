﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;

namespace Mx.Life
{
	public sealed class GameCellMouseDownEventArgs : RoutedEventArgs
	{
		public GameCellMouseDownEventArgs(ulong column, ulong row, MouseButton changedButton)
		{
			Column = column;
			Row = row;
			ChangedButton = changedButton;
		}

		public ulong Column
		{
			get;
			private set;
		}
		
		public ulong Row
		{
			get;
			private set;
		}

		public MouseButton ChangedButton
		{
			get;
			private set;
		}
	}
}
