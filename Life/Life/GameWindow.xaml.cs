﻿using Mx.Life.Game;
using Mx.Life.Storage;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace Mx.Life
{
	public sealed partial class GameWindow : Window
	{
		private const ulong MinimumSpan = 100;

		private Gameplay _gameplay;
		private CancellationTokenSource _gameplayStopper;
		private Task _gameplayTask; 

		static GameWindow()
		{
			CommandManager.RegisterClassCommandBinding(
				typeof(GameWindow),
				new CommandBinding(ConfigureCommand, ConfigureCommand_Executed));
			CommandManager.RegisterClassCommandBinding(
				typeof(GameWindow),
				new CommandBinding(LaunchCommand, LaunchCommand_Executed, LaunchCommand_CanExecute));
			CommandManager.RegisterClassCommandBinding(
				typeof(GameWindow),
				new CommandBinding(StoreCommand, StoreCommand_Executed, StoreCommand_CanExecute));
			CommandManager.RegisterClassCommandBinding(
				typeof(GameWindow),
				new CommandBinding(RestoreCommand, RestoreCommand_Executed));

			EventManager.RegisterClassHandler(
				typeof(GameWindow),
				GameboardView.CellMouseDownEvent,
				new EventHandler<GameCellMouseDownEventArgs>(GameCell_MouseDown));
		}

		public GameWindow()
		{
			DataContext = this;
			InitializeComponent();
		}

		private void InitializeGameCommon(Func<Gameboard> gameboardFactory)
		{
			StopGameplay();

			var initialBoard = gameboardFactory();

			_gameplay = new Gameplay(initialBoard);
			_gameplayStopper = new CancellationTokenSource();

			SetValue(GameboardPropertyKey, initialBoard);
		}

		private void InitializeGame()
		{
			InitializeGameCommon(() =>
				{
					var rangeBegin = (ulong.MaxValue - MinimumSpan) / 2;
					var initialRange = Tuple.Create(rangeBegin, rangeBegin + MinimumSpan - 1);
					
					return new Gameboard(initialRange, initialRange, new MemoryStorage());
				});
		}

		private void InitializeGame(IStorage storage)
		{
			// If restored storage has no live cells
			// initialize game by default.
			if (!storage.Any())
			{
				InitializeGame();
				return;
			}

			InitializeGameCommon(() =>
				{
					// Need to traverse storage to find out range of gameboard.

					var leftCol = ulong.MaxValue;
					var rightCol = 0ul;
					var topRow = ulong.MaxValue;
					var bottomRow = 0ul;

					foreach (var key in storage)
					{
						leftCol = Math.Min(leftCol, key.Column);
						rightCol = Math.Max(rightCol, key.Column);
						topRow = Math.Min(topRow, key.Row);
						bottomRow = Math.Max(bottomRow, key.Row);
					}

					return new Gameboard(
						Tuple.Create(leftCol, rightCol),
						Tuple.Create(topRow, bottomRow),
						storage);
				});
		}

		private void StopGameplay()
		{
			if (_gameplayStopper != null)
			{
				_gameplayStopper.Cancel();
				_gameplayStopper = null;

				if (_gameplayTask != null)
				{
					_gameplayTask.Wait();
				}
			}
		}
		
		private static void ConfigureCommand_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			var window = (GameWindow)sender;

			window.InitializeGame();
			window.SetValue(IsConfigurationModePropertyKey, true);
		}

		private static void GameCell_MouseDown(object sender, GameCellMouseDownEventArgs e)
		{
			var window = (GameWindow)sender;
			if (!window.IsConfigurationMode)
			{
				return;
			}

			var storageKey = new StorageKey(e.Column, e.Row);
			var gameboard = window.Gameboard;

			switch (e.ChangedButton)
			{
				case MouseButton.Left:
					{
						window.Gameboard.Storage[storageKey] = true;
						break;
					}
				case MouseButton.Right:
					{
						window.Gameboard.Storage[storageKey] = false;
						break;
					}
				default:
					{
						return;
					}
			}

			// Renew gameboard to show changes.
			gameboard = new Gameboard(gameboard.ColumnRange, gameboard.RowRange, gameboard.Storage);
			window.SetValue(GameboardPropertyKey, gameboard);
		}

		private static void LaunchCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
		{
			var window = (GameWindow)sender;
			e.CanExecute = window.IsConfigurationMode;
		}

		private static void LaunchCommand_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			var window = (GameWindow)sender;
			window.LaunchGame();
		}

		private void LaunchGame()
		{
			SetValue(IsConfigurationModePropertyKey, false);
			
			_gameplayTask = new Task(gameplayObjects =>
				{
					var tuple = (Tuple<Gameplay, CancellationToken>)gameplayObjects;
					var gameplay = tuple.Item1;
					var cancellationToken = tuple.Item2;
					
					foreach (var gameboard in gameplay.Run(cancellationToken))
					{
						// Use async invokation and wait loop to prevent deadlock
						// when UI thread waiting for task completion.
						var op = Dispatcher.BeginInvoke(new Action(() => SetValue(GameboardPropertyKey, gameboard)));
						while (op.Status != DispatcherOperationStatus.Completed && !cancellationToken.IsCancellationRequested)
						{
							Thread.Sleep(20);
						}

						Thread.Sleep(100);
					}
				},
				Tuple.Create(_gameplay, _gameplayStopper.Token));

			_gameplayTask.Start();
		}

		private static void StoreCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
		{
			var window = (GameWindow)sender;
			e.CanExecute = window.Gameboard != null;
		}

		private static void StoreCommand_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			var window = (GameWindow)sender;

			if (!window.IsConfigurationMode)
			{
				window.StopGameplay();
			}

			Mouse.OverrideCursor = Cursors.Wait;
			try
			{
				window.Gameboard.Storage.Backup();
			}
			finally
			{
				Mouse.OverrideCursor = null;
			}
		}

		private static void RestoreCommand_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			var window = (GameWindow)sender;

			if (!window.IsConfigurationMode)
			{
				window.StopGameplay();
			}

			var storage = new MemoryStorage();
			var isRestored = false;

			Mouse.OverrideCursor = Cursors.Wait;
			try
			{
				isRestored = storage.Restore();
			}
			finally
			{
				Mouse.OverrideCursor = null;
			}

			if (isRestored)
			{
				window.InitializeGame(storage);
				window.LaunchGame();
			}
			else
			{
				MessageBox.Show(
					"Unable to restore game. " +
					"This may be a kind of situation when storage is unavailable. " +
					"Make sure that you have stored a game previously.");
			}
		}

		#region Commands

		public static readonly ICommand ConfigureCommand = new RoutedUICommand("Configure", "Configure", typeof(GameWindow));
		public static readonly ICommand LaunchCommand = new RoutedUICommand("Launch", "Launch", typeof(GameWindow));

		public static readonly ICommand StoreCommand = new RoutedUICommand("Store", "Store", typeof(GameWindow));
		public static readonly ICommand RestoreCommand = new RoutedUICommand("Restore", "Restore", typeof(GameWindow));

		#endregion

		#region Gameboard Property

		private static readonly DependencyPropertyKey GameboardPropertyKey = DependencyProperty.RegisterReadOnly(
			"Gameboard", typeof(Gameboard), typeof(GameWindow),
			new PropertyMetadata(null, (d, e) => CommandManager.InvalidateRequerySuggested()));

		public static readonly DependencyProperty GameboardProperty = GameboardPropertyKey.DependencyProperty;

		public Gameboard Gameboard
		{
			get
			{
				return (Gameboard)GetValue(GameboardProperty);
			}
		}

		#endregion

		#region IsConfigurationMode Property

		private static readonly DependencyPropertyKey IsConfigurationModePropertyKey = DependencyProperty.RegisterReadOnly(
			"IsConfigurationMode", typeof(bool), typeof(GameWindow), 
			new PropertyMetadata(false, (d, e) => CommandManager.InvalidateRequerySuggested()));

		public static readonly DependencyProperty IsConfigurationModeProperty = IsConfigurationModePropertyKey.DependencyProperty;

		public bool IsConfigurationMode
		{
			get
			{
				return (bool)GetValue(IsConfigurationModeProperty);
			}
		}

		#endregion
	}
}
