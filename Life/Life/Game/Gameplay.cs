﻿using Mx.Life.Storage;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Mx.Life.Game
{
	internal sealed class Gameplay
	{
		private const ulong InitialSpan = 100;

		private Gameboard _gameboard;

		public Gameplay(Gameboard initialGameboard)
		{
			if (initialGameboard == null)
			{
				throw new ArgumentNullException("initialGameboard");
			}

			_gameboard = initialGameboard;
		}

		public IEnumerable<Gameboard> Run(CancellationToken cancellationToken)
		{
			while (!cancellationToken.IsCancellationRequested)
			{
				var nextStorage = new MemoryStorage();
				var nextColumnRange = _gameboard.ColumnRange;
				var nextRowRange = _gameboard.RowRange;

				// Traversing live cell keys while not cancelled.
				foreach (var key in _gameboard.Storage)
				{
					if (cancellationToken.IsCancellationRequested)
					{
						yield break;
					}

					var neighbourCount = 0;

					// Counting neighbours for live cell and also if neighbour
					// is a dead cell checking whether it should become alive.

					foreach (var neighbourKey in SelectNeighbourKeys(key))
					{
						if (_gameboard.Storage[neighbourKey])
						{
							neighbourCount++;
						}
						else if (!nextStorage[neighbourKey])
						{
							var deadNeighbourNeighbourCount = SelectNeighbourKeys(neighbourKey)
								.Where(k => _gameboard.Storage[k])
								.Take(4)
								.Count();

							if (deadNeighbourNeighbourCount == 3)
							{
								nextStorage[neighbourKey] = true;
								EnsureRange(neighbourKey, ref nextColumnRange, ref nextRowRange);
							}
						}
					}

					if (neighbourCount == 2 || neighbourCount == 3)
					{
						nextStorage[key] = true;
						EnsureRange(key, ref nextColumnRange, ref nextRowRange);
					}
				}

				_gameboard = new Gameboard(nextColumnRange, nextRowRange, nextStorage);

				yield return _gameboard;
			}
		}

		private void EnsureRange(StorageKey newLiveCell, ref Tuple<ulong, ulong> columnRange, ref Tuple<ulong, ulong> rowRange)
		{
			columnRange = Tuple.Create(
				Math.Min(newLiveCell.Column, columnRange.Item1),
				Math.Max(newLiveCell.Column, columnRange.Item2));

			rowRange = Tuple.Create(
				Math.Min(newLiveCell.Row, rowRange.Item1),
				Math.Max(newLiveCell.Row, rowRange.Item2));
		}

		public IEnumerable<StorageKey> SelectNeighbourKeys(StorageKey key)
		{
			var isFirstColumn = key.Column == 0;
			var isLastColumn = key.Column == ulong.MaxValue;
			var isFirstRow = key.Row == 0;
			var isLastRow = key.Row == ulong.MaxValue;

			if (!isFirstColumn)
			{
				yield return new StorageKey(key.Column - 1, key.Row);

				if (!isFirstRow)
				{
					yield return new StorageKey(key.Column - 1, key.Row - 1);
				}
				if (!isLastRow)
				{
					yield return new StorageKey(key.Column - 1, key.Row + 1);
				}
			}

			if (!isFirstRow)
			{
				yield return new StorageKey(key.Column, key.Row - 1);
			}
			if (!isLastRow)
			{
				yield return new StorageKey(key.Column, key.Row + 1);
			}

			if (!isLastColumn)
			{
				yield return new StorageKey(key.Column + 1, key.Row);

				if (!isFirstRow)
				{
					yield return new StorageKey(key.Column + 1, key.Row - 1);
				}
				if (!isLastRow)
				{
					yield return new StorageKey(key.Column + 1, key.Row + 1);
				}
			}
		}
	}
}
