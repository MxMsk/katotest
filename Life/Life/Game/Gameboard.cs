﻿using Mx.Life.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mx.Life.Game
{
	using UInt64Tuple = Tuple<ulong, ulong>;

	public sealed class Gameboard
	{
		public Gameboard(UInt64Tuple columnRange, UInt64Tuple rowRange, IStorage storage)
		{
			if (columnRange.Item1 > columnRange.Item2)
			{
				throw new ArgumentException("columnRange");
			}
			if (rowRange.Item1 > rowRange.Item2)
			{
				throw new ArgumentException("rowRange");
			}

			ColumnRange = columnRange;
			RowRange = rowRange;
			Storage = storage;
		}

		public UInt64Tuple ColumnRange
		{
			get;
			private set;
		}

		public UInt64Tuple RowRange
		{
			get;
			private set;
		}

		public IStorage Storage
		{
			get;
			private set;
		}
	}
}
