﻿using Mx.Life.Game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Mx.Life
{
	public sealed class GameboardView : Control
	{
		private const double CellSize = 10;

		private Pen _foregroundPen;

		static GameboardView()
		{
			ClipToBoundsProperty.OverrideMetadata(
				typeof(GameboardView),
				new FrameworkPropertyMetadata(true));
			ForegroundProperty.OverrideMetadata(
				typeof(GameboardView),
				new FrameworkPropertyMetadata((d, e) => ((GameboardView)d).UpdateForegroundPen()));
		}

		public GameboardView()
		{
			UpdateForegroundPen();
		}

		private void UpdateForegroundPen()
		{
			var foreground = Foreground;

			if (foreground == null)
			{
				_foregroundPen = null;
				return;
			}

			var pen = new Pen(foreground, 0.4);
			if (pen.CanFreeze)
			{
				pen.Freeze();
			}
			_foregroundPen = pen;
		}

		protected override Size MeasureOverride(Size constraint)
		{
			// The value to use for undefined measure, i.e. default game area.
			const double defaultLength = 600;

			return new Size(
				double.IsNaN(Width) ? Math.Min(constraint.Width, defaultLength) : 0,
				double.IsNaN(Height) ? Math.Min(constraint.Height, defaultLength) : 0);
		}

		protected override void OnRender(DrawingContext drawingContext)
		{
			drawingContext.DrawRectangle(Background, null, new Rect(RenderSize));

			if (_foregroundPen == null)
			{
				return;
			}

			var gameboard = Gameboard;
			if (gameboard == null)
			{
				return;
			}

			// Collect and render points of live cells.

			Size cellSize;
			ulong columnSpan;
			ulong rowSpan;

			CalcCellMetrics(out cellSize, out columnSpan, out rowSpan);

			var cellPixels = new HashSet<Point>();
			foreach (var cell in gameboard.Storage)
			{
				var x = (cell.Column - gameboard.ColumnRange.Item1) * cellSize.Width;
				var y = (cell.Row - gameboard.RowRange.Item1) * cellSize.Height;
				cellPixels.Add(new Point(Math.Round(x), Math.Round(y)));
			}

			foreach (var cellPixel in cellPixels)
			{
				drawingContext.DrawRectangle(_foregroundPen.Brush, null, new Rect(cellPixel, cellSize));
			}

			// Render grid if it is enabled.
			// Subject for optimization when grid is huge.

			if (!ShowGridLines)
			{
				return;
			}

			drawingContext.PushOpacity(0.5);

			for (ulong i = 0; i <= columnSpan; i++)
			{
				var x = (i + 1) * cellSize.Width;
				drawingContext.DrawLine(_foregroundPen, new Point(x, 0), new Point(x, RenderSize.Height));
			}

			for (ulong j = 0; j <= rowSpan; j++)
			{
				var y = (j + 1) * cellSize.Height;
				drawingContext.DrawLine(_foregroundPen, new Point(0, y), new Point(RenderSize.Width, y));
			}

			drawingContext.Pop();
		}

		private void CalcCellMetrics(out Size cellSize, out ulong columnSpan, out ulong rowSpan)
		{
			var gameboard = Gameboard;

			columnSpan = gameboard.ColumnRange.Item2 - gameboard.ColumnRange.Item1;
			rowSpan = gameboard.RowRange.Item2 - gameboard.RowRange.Item1;

			var columnCount = Math.Round(1d + columnSpan);
			var rowCount = Math.Round(1d + rowSpan);

			cellSize = new Size(ActualWidth / columnCount, ActualHeight / rowCount);
		}

		protected override void OnMouseDown(MouseButtonEventArgs e)
		{
			base.OnMouseDown(e);

			var gameboard = Gameboard;
			if (gameboard == null)
			{
				return;
			}

			// Find out whether the Mouse pressed under gameboard cell.
			// If true, raise appropriate event.

			var pt = e.GetPosition(this);

			Size cellSize;
			ulong columnSpan;
			ulong rowSpan;

			CalcCellMetrics(out cellSize, out columnSpan, out rowSpan);

			var columnApx = Math.Floor(pt.X / cellSize.Width);
			var rowApx = Math.Floor(pt.Y / cellSize.Height);

			ulong column;
			ulong row;

			try
			{
				checked
				{
					column = Math.Min((ulong)columnApx, columnSpan) + gameboard.ColumnRange.Item1;
					row = Math.Min((ulong)rowApx, rowSpan) + gameboard.RowRange.Item1;
				}
			}
			catch (OverflowException)
			{
				return;
			}

			var args = new GameCellMouseDownEventArgs(column, row, e.ChangedButton)
			{
				RoutedEvent = GameboardView.CellMouseDownEvent,
			};
			RaiseEvent(args);
		}

		#region Gameboard Property

		public static readonly DependencyProperty GameboardProperty = DependencyProperty.Register(
			"Gameboard", typeof(Gameboard), typeof(GameboardView),
			new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.AffectsRender));

		public Gameboard Gameboard
		{
			get
			{
				return (Gameboard)GetValue(GameboardProperty);
			}
			set
			{
				SetValue(GameboardProperty, value);
			}
		}

		#endregion

		#region ShowGridLines Property

		public static readonly DependencyProperty ShowGridLinesProperty = DependencyProperty.Register(
			"ShowGridLines", typeof(bool), typeof(GameboardView),
			new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.AffectsRender));

		public bool ShowGridLines
		{
			get
			{
				return (bool)GetValue(ShowGridLinesProperty);
			}
			set
			{
				SetValue(ShowGridLinesProperty, value);
			}
		}

		#endregion

		#region CellMouseDown Event

		public static readonly RoutedEvent CellMouseDownEvent = EventManager.RegisterRoutedEvent(
			"CellMouseDown", RoutingStrategy.Bubble, typeof(EventHandler<GameCellMouseDownEventArgs>), typeof(GameboardView));

		public event EventHandler<GameCellMouseDownEventArgs> CellMouseDown
		{
			add
			{
				AddHandler(CellMouseDownEvent, value);
			}
			remove
			{
				RemoveHandler(CellMouseDownEvent, value);
			}
		}

		#endregion
	}
}